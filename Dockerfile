FROM debian:bookworm-slim


LABEL authors="Patrick Jentsch <p.jentsch@uni-bielefeld.de>"


ENV LANG=C.UTF-8


##############################################################################
# Install system dependencies                                                #
##############################################################################
RUN apt-get update \
 && apt-get install --no-install-recommends --yes \
    subversion \
    autoconf \
    bison \
    flex \
    gcc \
    make \
    pkg-config \
    libc6-dev \
    libncurses-dev \
    libpcre2-8-0 \
    libpcre2-dev \
    libglib2.0-dev \
    libreadline-dev \
 && rm --recursive /var/lib/apt/lists/*


##############################################################################
# Install IMS Open Corpus Workbench and Perl CWB package                     #
##############################################################################
ENV CWB_PLATFORM=linux-64
ENV CWB_SITE=standard
ENV CWB_REVISION=1887
RUN svn checkout "http://svn.code.sf.net/p/cwb/code/cwb/trunk@${CWB_REVISION}" cwb \
 && cd cwb \
 && make clean PLATFORM=${CWB_PLATFORM} SITE=${CWB_SITE} \
 && make depend PLATFORM=${CWB_PLATFORM} SITE=${CWB_SITE} \
 && make all PLATFORM=${CWB_PLATFORM} SITE=${CWB_SITE} \
 && make install PLATFORM=${CWB_PLATFORM} SITE=${CWB_SITE} \
 && make realclean PLATFORM=${CWB_PLATFORM} SITE=${CWB_SITE} \
 && cd - > /dev/null \
 && rm -r cwb

RUN svn checkout "http://svn.code.sf.net/p/cwb/code/perl/trunk/CWB@${CWB_REVISION}" perl-cwb \
 && cd perl-cwb \
 && perl Makefile.PL \
 && make \
 && make test \
 && make install \
 && cd - > /dev/null \
 && rm -r perl-cwb
