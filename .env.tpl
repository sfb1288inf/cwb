# DEFAULT: ./cqpserver.init
# NOTE: Use `.` as <project-basedir>
# HOST_CWB_CQPSERVER_INIT_FILE=

# DEFAULT: ./data
# NOTE: Use `.` as <project-basedir>
# HOST_CWB_DATA_DIR=

# DEFAULT: ./files
# NOTE: Use `.` as <project-basedir>
# HOST_CWB_FILES_DIR=

# DEFAULT: ./registry
# NOTE: Use `.` as <project-basedir>
# HOST_CWB_REGISTRY_DIR=
