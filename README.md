# CWB - Open Corpus Workbench

![release badge](https://gitlab.ub.uni-bielefeld.de/sfb1288inf/cwb/-/badges/release.svg)
![pipeline badge](https://gitlab.ub.uni-bielefeld.de/sfb1288inf/cwb/badges/master/pipeline.svg?ignore_skipped=true)

This repository provides an easily deployable Open Corpus Workbench Docker image.

## Software used in this pipeline implementation

- Official Debian Docker image (bookworm-slim): https://hub.docker.com/_/debian
  - Software from Debian Bookworm's free repositories
- Open Corpus Workbench (Revision 1887): https://sourceforge.net/p/cwb/code/1853/tree/


## Installation

- Install [Docker](https://docs.docker.com/engine/install/) and [Docker Compose](https://docs.docker.com/compose/install/).
- Clone this repository: `git clone https://gitlab.ub.uni-bielefeld.de/sfb1288inf/cwb.git`
- Change to the repository directory: `cd cwb`
- For `cqp` and `cqpserver`:
  - `mkdir -p volumes/cwb/root/vrt_files`
  - `mkdir -p volumes/cwb/corpora/data`
  - `mkdir -p volumes/cwb/usr/local/share/cwb/registry`
- For `cqpserver` only:
  - `mkdir -p volumes/cwb/root/.cwb`
  - `touch volumes/cwb/root/.cwb/cqpserver.init`
  - `echo "host *;" >> volumes/cwb/root/.cwb/cqpserver.init`
  - `echo "user USERNAME \"PASSWORD\";" >> volumes/cwb/root/.cwb/cqpserver.init`
    - Replace `USERNAME` and `PASSWORD` with actual values

## Usage

You can either start an interactive cqp session in your terminal or start a cqpserver for interaction via [CQi](https://cwb.sourceforge.io/documentation.php#cqi).

**Start an interactive cqp session**: `docker compose -f docker-compose.cqp.yml run --rm cwb`

**cqpserver**:
- Start: `docker compose -f docker-compose.cqpserver.yml up`
- Stop:
  - `ctrl+c` and wait for your command prompt
  - `docker compose -f docker-compose.cqpserver.yml down`

**cqpserver in background**
- Start in background: `docker compose -f docker-compose.cqpserver.yml up -d`
- Stop: `docker compose -f docker-compose.cqpserver.yml down`
